package com.bit.project.aplikasi_user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class AplikasiUserApplication {

    public static void main(String[] args) {
        SpringApplication.run(AplikasiUserApplication.class, args);
    }

}
