package com.bit.project.aplikasi_user.controller;

import com.bit.project.aplikasi_user.dao.PenggunaDao;
import com.bit.project.aplikasi_user.entity.Pengguna;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Stefanus
 */
@RestController
@RequestMapping("/pengguna")
public class PenggunaController {

    @Autowired
    private PenggunaDao penggunaDao;

    @GetMapping("/")
    public List<Pengguna> getAll() {
        return penggunaDao.findAll();
    }

    @PostMapping("/")
    public Pengguna add(@Valid @RequestBody Pengguna pengguna) {
        return penggunaDao.save(pengguna);
    }
}
