package com.bit.project.aplikasi_user.dao;

import com.bit.project.aplikasi_user.entity.Pengguna;
import java.util.List;

/**
 *
 * @author Stefanus
 */
public interface PenggunaServiceDao {

    List<Pengguna> findAll();

    Pengguna findById(String id);

    Pengguna update(String id, Pengguna pengguna);

    Pengguna create(Pengguna pengguna);

    void delete(String id);
}
