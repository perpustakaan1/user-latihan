package com.bit.project.aplikasi_user.dao;

import com.bit.project.aplikasi_user.entity.Pengguna;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Stefanus
 */
public interface PenggunaDao extends JpaRepository<Pengguna, String> {
	List<Pengguna> findByEmail(String email);
}
